<?php

namespace App;

use App\Abstracts\DefaultModelsAbstract;
use Illuminate\Database\Eloquent\Builder;

class Key extends DefaultModelsAbstract
{
    const TABLE = 'keys';

    /**
     * @param string $title
     * @return Key
     */
    public static function createFactory(string $title): Key
    {
        $key = new static();
        $key->{self::TITLE} = $title;
        $key->save();

        return $key;
    }

    /**
     * @param string $title
     * @return Key
     */
    public function updateFactory(string $title): Key
    {
        $this->update([self::TITLE => $title]);

        return $this;
    }

    /**
     * @param Builder $builder
     * @param array $ids
     * @return Builder
     */
    public function scopeWhereIdIn(Builder $builder, array $ids): Builder
    {
        return $builder->whereIn(self::ID, $ids);
    }
}
