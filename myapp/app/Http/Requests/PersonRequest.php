<?php

namespace App\Http\Requests;

use App\Key;
use App\Person;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PersonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            Person::FIRST_NAME => 'required|string|min:3',
            Person::LAST_NAME => 'required|string|min:3',
            'keys' => 'array',
            'keys.*' => 'exists:' . Key::TABLE . ',' . Key::ID
        ];
    }

    /**
     * @param $validator
     */
    public function withValidator($validator)
    {
        if (!$validator->fails()) {
            if ($this->has('keys') && count($this->get('keys'))) {
                $this->keys = array_unique($this->keys);
            } else {
                $this->merge(['keys' => []]);
            }
            \Illuminate\Support\Facades\Validator::make($this->all(), [
                Person::FIRST_NAME =>
                    [
                        Rule::unique(Person::TABLE)->where(function ($query) {
                            return $query->where(Person::FIRST_NAME, $this->get(Person::FIRST_NAME))
                                ->where(Person::FIRST_NAME, $this->get(Person::FIRST_NAME));
                        })->ignore(optional($this->person)->{Person::ID}),
                    ]
            ])->validate();
        }
    }
}
