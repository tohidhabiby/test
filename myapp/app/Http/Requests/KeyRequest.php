<?php

namespace App\Http\Requests;

use App\Key;
use Illuminate\Foundation\Http\FormRequest;

class KeyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            Key::TITLE => sprintf(
                'required|string|min:3|unique:%s,%s,%s',
                Key::TABLE,
                Key::TITLE,
                optional($this->key)->{Key::ID}
            )
        ];
    }
}
