<?php

namespace App\Http\Requests;

use App\Company;
use App\Key;
use Illuminate\Foundation\Http\FormRequest;

class CompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            Company::TITLE => sprintf(
                'required|string|unique:%s,%s,%s',
                Company::TABLE,
                Company::TITLE,
                optional($this->company)->{Company::ID}
            ),
            'keys' => 'array',
            'keys.*' => 'exists:' . Key::TABLE . ',' . Key::ID
        ];
    }

    /**
     * @param $validator
     */
    public function withValidator($validator)
    {
        if (!$validator->fails()) {
            if ($this->has('keys') && count($this->get('keys'))) {
                $this->keys = array_unique($this->keys);
            } else {
                $this->merge(['keys' => []]);
            }
        }
    }
}
