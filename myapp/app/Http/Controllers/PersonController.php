<?php

namespace App\Http\Controllers;


use App\Filters\PersonFilters;
use App\Http\Requests\PersonRequest;
use App\Person;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;

class PersonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param PersonFilters $filters
     * @param Request $request
     * @return View
     */
    public function index(PersonFilters $filters, Request $request)
    {
        if ($request->ajax()) {
            return response()->json(Person::filter($filters)->get()->toJson());
        }
        return view('People.index')
            ->with(
                'links',
                ['person' => route('people.index'), 'list' => route('people.index')]
            )
            ->with('people', Person::filter($filters)->paginate());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create()
    {
        return view('People.manage')
            ->with(
                'links',
                [
                    'person' => route('people.index'),
                    'create' => route('people.create')
                ]
            )
            ->with('titlePage', 'Create Person')
            ->with('route', ['people.store'])
            ->with('method', 'POST');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PersonRequest $request
     * @return RedirectResponse
     */
    public function store(PersonRequest $request)
    {
        $person = Person::createFactory($request->get(Person::FIRST_NAME), $request->get(Person::LAST_NAME));
        $person->keys()->sync($request->get('keys'));
        Session::flash('message', 'Person created successfully.');
        Session::flash('alert-class', 'alert-success');

        return redirect()->action('PersonController@index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Person $person
     * @return View
     */
    public function edit(Person $person)
    {
        return view('People.manage')
            ->with('person', $person)
            ->with(
                'links',
                [
                    'league' => route('people.index'),
                    'edit' => route('people.edit', $person)
                ]
            )
            ->with('titlePage', 'Edit ' . ucwords($person->getFullNameAttribute()))
            ->with('route', ['people.update', $person->{Person::ID}])
            ->with('method', 'PATCH');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PersonRequest $request
     * @param Person $person
     * @return RedirectResponse
     */
    public function update(PersonRequest $request, Person $person)
    {
        $person->updateFactory($request->get(Person::FIRST_NAME), $request->get(Person::LAST_NAME));
        $person->keys()->sync($request->get('keys'));
        Session::flash('message', 'Person updated successfully.');
        Session::flash('alert-class', 'alert-success');

        return redirect()->action('PersonController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Person $person
     * @return RedirectResponse
     */
    public function destroy(Person $person)
    {
        try {
            $person->keys()->sync([]);
            $person->delete();
            Session::flash('message', 'Person deleted successfully.');
            Session::flash('alert-class', 'alert-success');
        } catch (Exception $exception) {
            Session::flash('message', 'System can not delete ' . $person->getFullNameAttribute() . '!');
            Session::flash('alert-class', 'alert-danger');
        }
        return redirect()->action('PersonController@index');
    }
}
