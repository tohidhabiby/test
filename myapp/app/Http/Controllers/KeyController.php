<?php

namespace App\Http\Controllers;

use App\Filters\DefaultFilters;
use App\Http\Requests\KeyRequest;
use App\Key;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;

class KeyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param DefaultFilters $filters
     * @param Request $request
     * @return View
     */
    public function index(DefaultFilters $filters, Request $request)
    {
        if ($request->ajax()) {
            return response()->json(Key::filter($filters)->get()->toJson());
        }
        return view('Keys.index')
            ->with(
                'links',
                ['key' => route('keys.index'), 'list' => route('keys.index')]
            )
            ->with('keys', Key::filter($filters)->paginate());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create()
    {
        return view('Keys.manage')
            ->with('links', ['key' => route('keys.index'), 'create' => route('keys.create')])
            ->with('titlePage', 'Create Key')
            ->with('route', ['keys.store'])
            ->with('method', 'POST');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param KeyRequest $request
     * @return RedirectResponse
     */
    public function store(KeyRequest $request)
    {
        Key::createFactory($request->get(Key::TITLE));
        Session::flash('message', 'Key created successfully.');
        Session::flash('alert-class', 'alert-success');

        return redirect()->action('KeyController@index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Key $key
     * @return View
     */
    public function edit(Key $key)
    {
        return view('Keys.manage')
            ->with('key', $key)
            ->with('links', ['key' => route('keys.index'), 'edit' => route('keys.edit', $key)])
            ->with('titlePage', 'Edit ' . ucwords($key->{Key::TITLE}))
            ->with('route', ['keys.update', $key->{Key::ID}])
            ->with('method', 'PATCH');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param KeyRequest $request
     * @param Key $key
     * @return RedirectResponse
     */
    public function update(KeyRequest $request, Key $key)
    {
        $key->updateFactory($request->get(Key::TITLE));
        Session::flash('message', 'Key updated successfully.');
        Session::flash('alert-class', 'alert-success');

        return redirect()->action('KeyController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Key $key
     * @return RedirectResponse
     */
    public function destroy(Key $key)
    {
        try {
            $key->delete();
            Session::flash('message', 'Key deleted successfully.');
            Session::flash('alert-class', 'alert-success');
        } catch (Exception $exception) {
            Session::flash('message', 'System can not delete ' . $key->{Key::TITLE} . '!');
            Session::flash('alert-class', 'alert-danger');
        }
        return redirect()->action('KeyController@index');
    }
}
