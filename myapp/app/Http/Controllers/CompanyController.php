<?php

namespace App\Http\Controllers;

use App\Company;
use App\Filters\CompanyFilters;
use App\Http\Requests\CompanyRequest;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param CompanyFilters $filters
     * @param Request $request
     * @return View
     */
    public function index(CompanyFilters $filters, Request $request)
    {
        if ($request->ajax()) {
            return response()->json(Company::filter($filters)->get()->toJson());
        }
        return view('Companies.index')
            ->with(
                'links',
                ['company' => route('companies.index'), 'list' => route('companies.index')]
            )
            ->with('companies', Company::filter($filters)->paginate());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create()
    {
        return view('Companies.manage')
            ->with(
                'links',
                [
                    'company' => route('companies.index'),
                    'create' => route('companies.create')
                ]
            )
            ->with('titlePage', 'Create Company')
            ->with('route', ['companies.store'])
            ->with('method', 'POST');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CompanyRequest $request
     * @return RedirectResponse
     */
    public function store(CompanyRequest $request)
    {
        $company = Company::createFactory($request->get(Company::TITLE));
        $company->keys()->sync($request->get('keys'));
        Session::flash('message', 'Company created successfully.');
        Session::flash('alert-class', 'alert-success');

        return redirect()->action('CompanyController@index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Company $company
     * @return View
     */
    public function edit(Company $company)
    {
        return view('Companies.manage')
            ->with('company', $company)
            ->with(
                'links',
                [
                    'league' => route('companies.index'),
                    'edit' => route('companies.edit', $company)
                ]
            )
            ->with('titlePage', 'Edit ' . ucwords($company->{Company::TITLE}))
            ->with('route', ['companies.update', $company->{Company::ID}])
            ->with('method', 'PATCH');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CompanyRequest $request
     * @param Company $company
     * @return RedirectResponse
     */
    public function update(CompanyRequest $request, Company $company)
    {
        $company->updateFactory($request->get(Company::TITLE));
        $company->keys()->sync($request->get('keys'));
        Session::flash('message', 'Company updated successfully.');
        Session::flash('alert-class', 'alert-success');

        return redirect()->action('CompanyController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Company $company
     * @return RedirectResponse
     */
    public function destroy(Company $company)
    {
        try {
            $company->keys()->sync([]);
            $company->delete();
            Session::flash('message', 'Company deleted successfully.');
            Session::flash('alert-class', 'alert-success');
        } catch (Exception $exception) {
            Session::flash('message', 'System can not delete ' . $company->{Company::TITLE} . '!');
            Session::flash('alert-class', 'alert-danger');
        }
        return redirect()->action('CompanyController@index');
    }
}
