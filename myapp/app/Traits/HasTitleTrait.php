<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;

trait HasTitleTrait
{
    /**
     * @param Builder $query
     * @param string $title
     * @return Builder
     */
    public function scopeWhereTitleIs(Builder $query, string $title): Builder
    {
        return $query->where('title', '=', $title);
    }

    /**
     * @param Builder $query
     * @param string $title
     * @return Builder
     */
    public function scopeWhereTitleLike(Builder $query, string $title): Builder
    {
        return $query->where('title', 'like', "%$title%");
    }

    /**
     * @param Builder $query
     * @param string $title
     * @return Builder
     */
    public function scopeWhereTitleStartsBy(Builder $query, string $title): Builder
    {
        return $query->where('title', 'like', "$title%");
    }
}
