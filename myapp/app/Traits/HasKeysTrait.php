<?php

namespace App\Traits;

use App\Key;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

trait HasKeysTrait
{
    /**
     * @return BelongsToMany
     */
    public function keys(): BelongsToMany
    {
        return $this->belongsToMany(Key::class);
    }

    /**
     * @param Builder $builder
     * @param string $title
     * @return Builder
     */
    public function scopeWhereKeyTitleLike(Builder $builder, string $title): Builder
    {
        return $builder->whereHas('keys', function (Builder $joinKey) use ($title) {
            return $joinKey->whereTitleLike($title);
        });
    }

    /**
     * @param Builder $builder
     * @param array $keyIds
     * @return Builder
     */
    public function scopeWhereHasKeys(Builder $builder, array $keyIds): Builder
    {
        foreach ($keyIds as $id) {
            $builder->whereHas('keys', function (Builder $joinKey) use ($id) {
                return $joinKey->where(Key::ID, $id);
            });
        }

        return $builder;
    }

    /**
     * @param Builder $builder
     * @param array $keyIds
     * @return Builder
     */
    public function scopeWhereKeysJustIn(Builder $builder, array $keyIds): Builder
    {
        $reverseKeys = Key::whereNotIn(Key::ID, $keyIds)->pluck(Key::ID)->toArray();
        if (empty($reverseKeys)) {
            return $builder->whereDoesntHave('keys');
        } else {
            return $builder->whereNotIn(self::ID,
                static::whereHas(
                    'keys',
                    function (Builder $joinKey) use ($reverseKeys) {
                        return $joinKey->whereIn(Key::ID, $reverseKeys);
                    }
                )->pluck('id')
            );
        }
    }
}
