<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;

trait GeneralModelTraits
{
    /**
     * Apply all relevant thread filters.
     *
     * @param Builder $query
     * @param  $filters
     * @return Builder
     */
    public function scopeFilter(Builder $query, $filters): Builder
    {
        return $filters->apply($query);
    }

    /**
     * @param Builder $query
     * @param array $ids
     * @return Builder
     */
    public function scopeWhereIdNotIn(Builder $query, array $ids = []) : Builder
    {
        return $query->whereNotIn(self::ID, $ids);
    }
}
