<?php
namespace App\Filters;

use Illuminate\Database\Eloquent\Builder;

class PersonFilters extends Filters
{
    /**
     * Registered filters to operate upon.
     *
     * @var array
     */
    protected $filters = ['firstName', 'lastName', 'notIn', 'hasKeys', 'keysJustIn'];

    /**
     * @param string $firstName
     * @return Builder
     */
    protected function firstName(string $firstName) : Builder
    {
        return $this->builder->whereFirstNameLike($firstName);
    }

    /**
     * @param string $lastName
     * @return Builder
     */
    protected function lastName(string $lastName) : Builder
    {
        return $this->builder->whereLastNameLike($lastName);
    }

    /**
     * @param array $notIn
     * @return Builder
     */
    protected function notIn(array $notIn) : Builder
    {
        return $this->builder->whereIdNotIn($notIn);
    }

    /**
     * @param string $keys
     * @return Builder
     */
    protected function hasKeys(string $keys): Builder
    {
        $keys = array_unique(explode(',', $keys));

        return $this->builder->whereHasKeys($keys);
    }

    /**
     * @param string $keys
     * @return Builder
     */
    protected function keysJustIn(string $keys): Builder
    {
        $keys = array_unique(explode(',', $keys));

        return $this->builder->whereKeysJustIn($keys);
    }
}
