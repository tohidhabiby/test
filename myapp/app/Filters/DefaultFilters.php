<?php

namespace App\Filters;

use Illuminate\Database\Eloquent\Builder;

class DefaultFilters extends Filters
{
    /**
     * Registered filters to operate upon.
     *
     * @var array
     */
    protected $filters = ['title', 'notIn'];

    /**
     * @param string $title
     * @return Builder
     */
    protected function title(string $title): Builder
    {
        return $this->builder->whereTitleLike($title);
    }

    /**
     * @param string $notIn
     * @return Builder
     */
    protected function notIn(string $notIn): Builder
    {
        $notIn = array_unique(explode(',', $notIn));
        return $this->builder->whereIdNotIn($notIn);
    }
}
