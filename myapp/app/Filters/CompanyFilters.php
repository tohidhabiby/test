<?php

namespace App\Filters;

use Illuminate\Database\Eloquent\Builder;

class CompanyFilters extends DefaultFilters
{
    /**
     * Registered filters to operate upon.
     *
     * @var array
     */
    protected $filters = ['title', 'notIn', 'hasKeys', 'keysJustIn'];

    /**
     * @param string $keys
     * @return Builder
     */
    protected function hasKeys(string $keys): Builder
    {
        $keys = array_unique(explode(',', $keys));

        return $this->builder->whereHasKeys($keys);
    }

    /**
     * @param string $keys
     * @return Builder
     */
    protected function keysJustIn(string $keys): Builder
    {
        $keys = array_unique(explode(',', $keys));

        return $this->builder->whereKeysJustIn($keys);
    }
}
