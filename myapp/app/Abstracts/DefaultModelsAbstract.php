<?php

namespace App\Abstracts;

use App\Contracts\GeneralModelInterface;
use App\Contracts\HasTitleInterface;
use App\Traits\GeneralModelTraits;
use App\Traits\HasTitleTrait;
use Illuminate\Database\Eloquent\Model;

abstract class DefaultModelsAbstract extends Model implements HasTitleInterface, GeneralModelInterface
{
    use HasTitleTrait, GeneralModelTraits;

    const ID = 'id';
    const TITLE = 'title';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $fillable = [self::TITLE];
}
