<?php

namespace App;

use App\Abstracts\DefaultModelsAbstract;
use App\Contracts\CompanyInterface;
use App\Traits\HasKeysTrait;

class Company extends DefaultModelsAbstract implements CompanyInterface
{
    use HasKeysTrait;

    const TABLE = 'companies';

    /**
     * @param string $title
     * @return CompanyInterface
     */
    public static function createFactory(string $title): CompanyInterface
    {
        $company = new static();
        $company->{self::TITLE} = $title;
        $company->save();

        return $company;
    }

    /**
     * @param string $title
     * @return CompanyInterface
     */
    public function updateFactory(string $title): CompanyInterface
    {
        $this->update([self::TITLE => $title]);

        return $this;
    }
}
