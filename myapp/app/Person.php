<?php

namespace App;

use App\Contracts\PersonInterface;
use App\Traits\GeneralModelTraits;
use App\Traits\HasKeysTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Person extends Model implements PersonInterface
{
    use GeneralModelTraits, HasKeysTrait;

    const TABLE = 'people';
    const ID = 'id';
    const FIRST_NAME = 'first_name';
    const LAST_NAME = 'last_name';

    protected $fillable = ['first_name', 'last_name'];

    protected $appends = ['full_name'];

    /**
     * @return string
     */
    public function getFullNameAttribute(): string
    {
        return ucwords($this->{self::FIRST_NAME}) . ' ' . ucwords($this->{self::LAST_NAME});
    }

    /**
     * @param Builder $builder
     * @param string $firstName
     * @return Builder
     */
    public function scopeWhereFirstNameLike(Builder $builder, string $firstName): Builder
    {
        return $builder->where(self::FIRST_NAME, 'like', '%' . $firstName . '%');
    }

    /**
     * @param Builder $builder
     * @param string $lastName
     * @return Builder
     */
    public function scopeWhereLastNameLike(Builder $builder, string $lastName): Builder
    {
        return $builder->where(self::LAST_NAME, 'like', '%' . $lastName . '%');
    }

    /**
     * @param string $firstName
     * @param string $lastName
     * @return PersonInterface
     */
    public static function createFactory(string $firstName, string $lastName): PersonInterface
    {
        $person = new static();
        $person->{self::FIRST_NAME} = $firstName;
        $person->{self::LAST_NAME} = $lastName;
        $person->save();

        return $person;
    }

    /**
     * @param string $firstName
     * @param string $lastName
     * @return PersonInterface
     */
    public function updateFactory(string $firstName, string $lastName): PersonInterface
    {
        $this->update([self::FIRST_NAME => $firstName, self::LAST_NAME => $lastName]);

        return $this;
    }
}
