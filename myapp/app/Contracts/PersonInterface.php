<?php

namespace App\Contracts;

use Illuminate\Database\Eloquent\Builder;

interface PersonInterface extends GeneralModelInterface,HasKeysInterface
{
    /**
     * @param string $firstName
     * @param string $lastName
     * @return PersonInterface
     */
    public static function createFactory(string $firstName, string $lastName): PersonInterface;

    /**
     * @param string $firstName
     * @param string $lastName
     * @return PersonInterface
     */
    public function updateFactory(string $firstName, string $lastName): PersonInterface;

    /**
     * @return string
     */
    public function getFullNameAttribute(): string;

    /**
     * @param Builder $builder
     * @param string $firstName
     * @return Builder
     */
    public function scopeWhereFirstNameLike(Builder $builder, string $firstName): Builder;

    /**
     * @param Builder $builder
     * @param string $lastName
     * @return Builder
     */
    public function scopeWhereLastNameLike(Builder $builder, string $lastName): Builder;
}
