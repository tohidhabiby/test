<?php

namespace App\Contracts;

use Illuminate\Database\Eloquent\Builder;

interface HasTitleInterface
{
    /**
     * @param Builder $query
     * @param string $title
     * @return Builder
     */
    public function scopeWhereTitleIs(Builder $query, string $title): Builder;

    /**
     * @param Builder $query
     * @param string  $title
     * @return Builder
     */
    public function scopeWhereTitleLike(Builder $query, string $title): Builder;

    /**
     * @param Builder $query
     * @param string $title
     * @return Builder
     */
    public function scopeWhereTitleStartsBy(Builder $query, string $title): Builder;
}
