<?php

namespace App\Contracts;

use Illuminate\Database\Eloquent\Builder;

interface GeneralModelInterface
{
    /**
     * @param Builder $query
     * @param $filters
     * @return Builder
     */
    public function scopeFilter(Builder $query, $filters): Builder;

    /**
     * @param Builder $query
     * @param array $ids
     * @return Builder
     */
    public function scopeWhereIdNotIn(Builder $query, array $ids = []): Builder;
}
