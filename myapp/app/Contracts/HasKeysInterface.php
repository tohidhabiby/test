<?php

namespace App\Contracts;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

interface HasKeysInterface
{
    /**
     * @return BelongsToMany
     */
    public function keys(): BelongsToMany;

    /**
     * @param Builder $builder
     * @param string $title
     * @return Builder
     */
    public function scopeWhereKeyTitleLike(Builder $builder, string $title): Builder;

    /**
     * @param Builder $builder
     * @param array $keyIds
     * @return Builder
     */
    public function scopeWhereHasKeys(Builder $builder, array $keyIds): Builder;

    /**
     * @param Builder $builder
     * @param array $keyIds
     * @return Builder
     */
    public function scopeWhereKeysJustIn(Builder $builder, array $keyIds): Builder;
}
