<?php

namespace App\Contracts;

use Illuminate\Database\Eloquent\Builder;

interface CompanyInterface extends HasKeysInterface
{
    /**
     * @param string $title
     * @return CompanyInterface
     */
    public static function createFactory(string $title): CompanyInterface;

    /**
     * @param string $title
     * @return CompanyInterface
     */
    public function updateFactory(string $title): CompanyInterface;

    /**
     * @param Builder $builder
     * @param array $keyIds
     * @return Builder
     */
    public function scopeWhereKeysJustIn(Builder $builder, array $keyIds): Builder;
}
