@extends('master')
@section('titlePage', $titlePage)
@section('content')
    <div class="col-lg-12">
        <div class="grid">
            <p class="grid-header">{{ $titlePage }}</p>
            <div class="grid-body">
                <div class="item-wrapper">
                    {!! Form::open(['route' => $route, 'method' => $method, 'class' => 'form-horizontal tasi-form']) !!}
                    <div class="row">
                        <div class="col-md-8 mx-auto">
                            <div class="row showcase_row_area">
                                <div class="col-md-3 showcase_text_area">
                                    <label for="inputType12">Title</label>
                                </div>
                                <div class="col-md-9 showcase_content_area">
                                    @if(isset($company))
                                        {!! Form::text(
                                            'title',
                                            $company->title,
                                            [
                                                'class' => 'form-control form-control-lg',
                                                'tabindex' => 1,
                                                'autofocus' => true,
                                                'required' => true
                                            ]
                                        ) !!}
                                    @else
                                        {!! Form::text(
                                            'title',
                                            null,
                                            [
                                                'class' => 'form-control form-control-lg',
                                                'tabindex' => 1,
                                                'autofocus' => true,
                                                'required' => true
                                            ]
                                        ) !!}
                                    @endif
                                    @error('title')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>


                            <div class="form-group">
                                {!! Form::label('keys', 'Requirements', ['class' => 'col-sm-2 control-label']) !!}
                                <div class="col-lg-10 checkboxes" id="addNewKeys">
                                    <div class="nav navbar-left">
                                        <button type="button" class="btn btn-white" onclick="emptyResult()" data-toggle="modal" data-target="#addKey" tabindex="4">
                                            Add Requirement
                                        </button>
                                    </div>
                                    <br>
                                    @if(isset($company))
                                        @foreach($company->keys as $myKey)
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <label>
                                                        {!! Form::checkbox(
                                                            'keys[]',
                                                            $myKey->id,
                                                            true,
                                                            [
                                                                'class' => 'keys',
                                                                'onchange' => 'getPeople()'
                                                            ]
                                                        ) . $myKey->title !!}
                                                    </label>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                                @error('keys')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>



                            <div class="row showcase_row_area">
                                {!! Form::submit('Submit', ['class' => 'btn btn-sm btn-primary pull-right']) !!}
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="addKey" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title left">Choose Requirement</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal tasi-form">
                        <div class="form-group">
                            {!! Form::label('searchTitle', 'Title', ['class' => 'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {!! Form::text(
                                    'searchTitle',
                                    null,
                                    [
                                        'onkeyup' => 'getKeys()',
                                        'class' => 'form-control',
                                        'id' => 'searchTitle'
                                    ]
                                ) !!}
                            </div>
                        </div>
                        <div id="searchResult"></div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <div class="col-lg-12" id="people">
        <div class="grid">
            <p class="grid-header">MATCHES PEOPLE LIST</p>
            <div class="item-wrapper">
                <div class="table-responsive">
                    <table class="table info-table table-striped">
                        <thead>
                        <tr>
                            <th style="text-align: center">ID</th>
                            <th style="text-align: center">First Name</th>
                            <th style="text-align: center">Last Name</th>
                        </tr>
                        </thead>
                        <tbody id="peopleResult">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script type="text/javascript">
        $(document).ready(function() {
            $("input:checkbox[class=keys]").change(getPeople());
        });
        function getPeople() {
            var APPAjax_URL = 'http://localhost/people?hasKeys=';
            var MYKEYS = [];
            var html = '';
            $("#peopleResult").html(html);
            $("input:checkbox[class=keys]:checked").each(function() {
                MYKEYS.push($(this).val());
            });
            if (MYKEYS.length == 0) {
                MYKEYS.push(0);
            }
            $.ajax({
                url : APPAjax_URL + MYKEYS ,
                type : 'GET',
                headers: {"Authorization": localStorage.getItem('token')},
                success: function(result){
                    result = JSON.parse(result);
                    var counter = 0;
                    $.each(result, function (index, value) {
                        html += "<tr><td style='text-align: center'>" + value['id'] +
                            "</td><td style='text-align: center'>" + value['first_name'] + "</td>" +
                            "<td style='text-align: center'>" + value['last_name'] + "</td></tr>";
                        counter++;
                    });
                    $("#peopleResult").html(html);
                }
            });
        }
        function emptyResult() {
            $("#searchResult").html('');
            $("#searchTitle").val('');
        }
        function getKeys() {
            var APPAjax_URL = 'http://localhost/keys?title=';
            var MYKEYS = [];
            var html = '';
            $("#searchResult").html(html);
            $("input:checkbox[class=keys]:checked").each(function() {
                MYKEYS.push($(this).val());
            });
            if (MYKEYS.length == 0) {
                MYKEYS.push(0);
            }
            $.ajax({
                url : APPAjax_URL + $("#searchTitle").val() + '&notIn=' + MYKEYS ,
                type : 'GET',
                headers: {"Authorization": localStorage.getItem('token')},
                success: function(result){
                    result = JSON.parse(result);
                    html += "<section class='panel'><table class='table table-striped'><thead><tr><th>ID</th><th>Title</th></tr></thead><tbody>";
                    $.each(result, function (index, value) {
                        html += "<tr onclick='addResult(this)' data-title='" + value['title'] + "'" +
                            "data-id='" + value['id'] + "'>" +
                            "<td>" + value['id'] + "</td><td>" + value['title'] + "</td></tr>";
                    });
                    html += "</tbody></table></section>";
                    $("#searchResult").html(html);
                }
            });
        }
        function addResult(myKey) {
            var id = myKey.getAttribute('data-id');
            var title = myKey.getAttribute('data-title');
            var htmlResult = "<div class='row' ><div class='col-lg-6'> " +
                "<label onclick='changeCheckbox(" + id + ")' >" +
                "<input name='keys[]' value='" + id + "' type='checkbox' class='keys' checked='checked' />" +
                title + "</label>";
            $("#addNewKeys").append(htmlResult);
            $('#addKey').modal('toggle');
            getPeople();
        }
        function changeCheckbox(ID) {
            if ($(".checkboxKey" + ID).is( ":checked" )) {
                $(".checkboxKey" + ID).prop("checked", false);
                $("#keyCheckbox" + ID).removeClass("checked");
            } else {
                $(".checkboxKey" + ID).prop("checked", true);
                $("#keyCheckbox" + ID).addClass("checked");
            }
        }
    </script>
@endsection
