@extends('master')
@section('titlePage', $titlePage)
@section('content')
    <div class="col-lg-12">
        <div class="grid">
            <p class="grid-header">{{ $titlePage }}</p>
            <div class="grid-body">
                <div class="item-wrapper">
                    {!! Form::open(['route' => $route, 'method' => $method, 'class' => 'form-horizontal tasi-form']) !!}
                    <div class="row">
                        <div class="col-md-8 mx-auto">
                            <div class="row showcase_row_area">
                                <div class="col-md-3 showcase_text_area">
                                    <label for="inputType12">Title</label>
                                </div>
                                <div class="col-md-9 showcase_content_area">
                                    @if(isset($key))
                                        {!! Form::text(
                                            'title',
                                            $key->title,
                                            [
                                                'class' => 'form-control form-control-lg',
                                                'tabindex' => 1,
                                                'autofocus' => true,
                                                'required' => true
                                            ]
                                        ) !!}
                                    @else
                                        {!! Form::text(
                                            'title',
                                            null,
                                            [
                                                'class' => 'form-control form-control-lg',
                                                'tabindex' => 1,
                                                'autofocus' => true,
                                                'required' => true
                                            ]
                                        ) !!}
                                    @endif
                                    @error('title')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="row showcase_row_area">
                                {!! Form::submit('Submit', ['class' => 'btn btn-sm btn-primary pull-right']) !!}
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
