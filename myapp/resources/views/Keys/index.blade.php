@extends('master')
@section('titlePage', 'Key List')

@section('content')
    <div class="col-lg-12">
    <div class="grid">
        <p class="grid-header">LEAGUE LIST</p>
        <div class="item-wrapper">
            <div class="table-responsive">
                <table class="table info-table table-striped">
                    <thead>
                    <tr>
                        <th style="text-align: center">ID</th>
                        <th style="text-align: center">Title</th>
                        <th style="text-align: center">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($keys as $key)
                    <tr>
                        <td style="text-align: center">{!! $key->{\App\Key::ID} !!}</td>
                        <td style="text-align: center">{!! $key->{\App\Key::TITLE} !!}</td>
                        <td style="text-align: center">
                            <a class="btn btn-info btn-xs" href="{!! url('/keys/' . $key->id . '/edit') !!}">
                                Edit<i class="fa fa-edit"></i>
                            </a>
                            {!! Form::open(
                            ['route' => ['keys.destroy', $key->id],
                             'method' => 'DELETE',
                              'style' => 'display:inline']
                              ) !!}
                                <input type="submit" style="display: none" id="submitButton{!! $key->id !!}" />
                            {!! Form::close() !!}
                            <button type="button"
                                    class="btn btn-danger btn-xs deleteButton" id="confirmButton{!! $key->id !!}">
                                Delete<i class="fa fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>
@endsection
@section('js')
    {!! Html::script('js/bootbox.min.js') !!}
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).on('click', '.deleteButton', function () {
                var id = this.id.replace('confirmButton', '');
                bootbox.confirm({
                    message: "Do you want to delete this item?", callback: function (result) {
                        if (result) {
                            $("#submitButton" + id).click();
                        }
                    }
                });
            });
        });
    </script>
@endsection
