<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>MyApp - @yield('titlePage')</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="../../../assets/vendors/iconfonts/mdi/css/materialdesignicons.css">
    <link rel="stylesheet" href="../../../assets/vendors/css/vendor.addons.css">
    <!-- endinject -->
    <!-- vendor css for this page -->
    <!-- End vendor css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="../../../assets/css/shared/style.css">
    <!-- endinject -->
    <!-- Layout style -->
    <link rel="stylesheet" href="../../../assets/css/demo_1/style.css">
    <!-- Layout style -->
    <link rel="shortcut icon" href="../../../assets/images/favicon.ico"/>
    @yield('css')
</head>
<body class="header-fixed">
<!-- partial:../../partials/_header.html -->
<!-- partial:partials/_header.html -->
<nav class="t-header" style="position: absolute">
    <div class="t-header-brand-wrapper">My App
    </div>
    <div class="t-header-content-wrapper">
        <div class="t-header-content">
            <button class="t-header-toggler t-header-mobile-toggler d-block d-lg-none">
                <i class="mdi mdi-menu"></i>
            </button>
            <ul class="nav ml-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#" id="appsDropdown" data-toggle="dropdown" aria-expanded="false">
                        <i class="mdi mdi-apps mdi-1x"></i>
                    </a>
                    <div class="dropdown-menu navbar-dropdown dropdown-menu-right" aria-labelledby="appsDropdown">
                        <div class="dropdown-header">
                            <h6 class="dropdown-title">My Profiles</h6>
                            <p class="dropdown-title-text mt-2">Please check my profiles</p>
                        </div>
                        <div class="dropdown-body border-top pt-0">
                            <a class="dropdown-grid" href="https://www.linkedin.com/in/tohid-habiby-4901b59a/">
                                <i class="grid-icon mdi mdi-linkedin mdi-2x"></i>
                                <span class="grid-tittle">LinkedIn</span>
                            </a>
                            <a class="dropdown-grid" href="https://github.com/tohidhabiby">
                                <i class="grid-icon mdi mdi-github-box mdi-2x"></i>
                                <span class="grid-tittle">GitHub</span>
                            </a>
                            <a class="dropdown-grid" href="https://stackoverflow.com/users/5451704/tohidhabiby">
                                <i class="grid-icon mdi mdi-stackoverflow mdi-2x"></i>
                                <span class="grid-tittle">StackOverFlow</span>
                            </a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!-- partial -->
<div class="page-body">
    <!-- partial:../../partials/_sidebar.html -->
    <div class="sidebar">
        <div class="user-profile">
            <div class="display-avatar animated-avatar">
                <img class="profile-img img-lg rounded-circle" src="../../../assets/images/profile/male/image_1.png"
                     alt="profile image">
            </div>
            <div class="info-wrapper">

                {{--<p class="user-name">{!! $currentUser->full_name !!}</p>--}}
                {{--<h6 class="display-income">$3,400,00</h6>--}}
            </div>
        </div>
        <ul class="navigation-menu">
            <li class="nav-category-divider">MAIN</li>
            <li>
                <a href="#sample-pages" data-toggle="collapse" aria-expanded="false">
                    <span class="link-title">Keys</span>
                    <i class="mdi mdi-flask link-icon"></i>
                </a>
                <ul class="collapse navigation-submenu" id="sample-pages">
                    <li>
                        <a href="{!! route('keys.index') !!}">Keys List</a>
                    </li>
                    <li>
                        <a href="{!! route('keys.create') !!}">Create a Key</a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="#ui-elements" data-toggle="collapse" aria-expanded="false">
                    <span class="link-title">People</span>
                    <i class="mdi mdi-bullseye link-icon"></i>
                </a>
                <ul class="collapse navigation-submenu" id="ui-elements">
                    <li>
                        <a href="{!! route('people.index') !!}">People List</a>
                    </li>
                    <li>
                        <a href="{!! route('people.create') !!}">Create a Person</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#sample-pages" data-toggle="collapse" aria-expanded="false">
                    <span class="link-title">Companies</span>
                    <i class="mdi mdi-flask link-icon"></i>
                </a>
                <ul class="collapse navigation-submenu" id="sample-pages">
                    <li>
                        <a href="{!! route('companies.index') !!}">Companies List</a>
                    </li>
                    <li>
                        <a href="{!! route('companies.create') !!}">Create a Company</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
    <!-- partial -->
    <div class="page-content-wrapper">
        <div class="page-content-wrapper-inner">
            <div class="viewport-header">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb has-arrow">
                        @foreach($links as $title => $link)
                        <li class="breadcrumb-item">
                            <a href="{!! $link !!}">{!! ucwords($title) !!}</a>
                        </li>
                        @endforeach
                    </ol>
                </nav>
            </div>
            <div class="content-viewport">
                <div class="row">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
    <!-- page content ends -->
</div>
<!-- SCRIPT LOADING START FORM HERE /////////////-->
<!-- plugins:js -->
<script src="../../../assets/vendors/js/core.js"></script>
<script src="../../../assets/vendors/js/vendor.addons.js"></script>
<!-- endinject -->
<!-- Vendor Js For This Page Ends-->
<!-- Vendor Js For This Page Ends-->
<!-- build:js -->
<script src="../../../assets/js/template.js"></script>
<!-- endbuild -->
@yield('js')
</body>
</html>
