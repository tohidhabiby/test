<?php

namespace Tests\Unit;

use App\Key;
use App\Person;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PersonUnitTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    /** @test */
    public function itShouldCreateNewPerson()
    {
        $firstName = $this->faker->firstName;
        $lastName = $this->faker->lastName;
        $person = Person::createFactory($firstName, $lastName);
        $this->assertEquals(1, Person::all()->count());
        $this->assertEquals($person->{Person::FIRST_NAME}, $firstName);
        $this->assertEquals($person->{Person::LAST_NAME}, $lastName);
    }

    /** @test */
    public function itShouldUpdatePerson()
    {
        $person = factory(Person::class)->create();
        $firstName = $this->faker->firstName;
        $lastName = $this->faker->lastName;
        $person->updateFactory($firstName, $lastName);
        $this->assertEquals(1, Person::all()->count());
        $this->assertEquals($person->{Person::FIRST_NAME}, $firstName);
        $this->assertEquals($person->{Person::LAST_NAME}, $lastName);
    }

    /** @test */
    public function itShouldFilterPeopleByFirstNameAndLastNameLike()
    {
        $firstPerson = factory(Person::class)
            ->create([Person::FIRST_NAME => 'sampleFirstName', Person::LAST_NAME => 'sampleLastName']);
        factory(Person::class)->create([Person::FIRST_NAME => 'xxxxxx', Person::LAST_NAME => 'xxxxxx']);
        $people = Person::whereFirstNameLike('ampleFirstNam')->get();
        $this->assertEquals(1, count($people));
        $this->assertTrue($people->first()->is($firstPerson));
        $people = Person::whereLastNameLike('ampleLastNam')->get();
        $this->assertEquals(1, count($people));
        $this->assertTrue($people->first()->is($firstPerson));
    }

    /** @test */
    public function itShouldReturnFullName()
    {
        $person = factory(Person::class)->create();
        $this->assertEquals(
            $person->getFullNameAttribute(),
            ucwords($person->{Person::FIRST_NAME}) . ' ' . ucwords($person->{Person::LAST_NAME})
        );
    }

    /** @test */
    public function itFilterPeopleWhereHasKeys()
    {
        $firstKey = factory(Key::class)->create();
        $secondKey = factory(Key::class)->create();
        $thirdKey = factory(Key::class)->create();
        $fourthKey = factory(Key::class)->create();
        $person = factory(Person::class)->create();
        $thirdPerson = factory(Person::class)->create();
        $person->keys()->sync([$firstKey->{Key::ID}, $secondKey->{Key::ID}]);
        $secondPerson = factory(Person::class)->create();
        $secondPerson->keys()->sync([$firstKey->{Key::ID}, $thirdKey->{Key::ID}]);
        $this->assertTrue(
            Person::whereHasKeys([$firstKey->{Key::ID}, $secondKey->{Key::ID}])->first()->is($person)
        );
        $this->assertEquals(Person::whereHasKeys([$firstKey->{Key::ID}])->get()->count(), 2);
        $this->assertEquals(Person::whereHasKeys([])->get()->count(), 3);
        $this->assertNull(Person::whereHasKeys([$firstKey->{Key::ID}, $fourthKey->{Key::ID}])->first());
        $this->assertTrue(
            Person::whereKeysJustIn([$firstKey->{Key::ID}, $secondKey->{Key::ID}, $thirdKey->{Key::ID}])
                ->get()
                ->first()
                ->is($person)
        );
        $this->assertTrue(
            Person::whereKeysJustIn([$thirdKey->{Key::ID}, $secondKey->{Key::ID}])->first()->is($thirdPerson)
        );
        $this->assertTrue(
            Person::whereKeysJustIn([$fourthKey->{Key::ID}, $thirdKey->{Key::ID}])->first()->is($thirdPerson)
        );
        $this->assertEquals(Person::whereKeysJustIn([$firstKey->{Key::ID}])->count(), 1);
        $this->assertTrue(
            Person::whereKeysJustIn([$firstKey->id, $thirdKey->id,$secondKey->{Key::ID}])
                ->get()
                ->first()
                ->is($person)
        );
    }
}
