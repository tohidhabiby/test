<?php

namespace Tests\Unit;

use App\Company;
use App\Key;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CompanyUnitTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    /** @test */
    public function itFilterCompaniesWhereHasKeys()
    {
        $firstKey = factory(Key::class)->create();
        $secondKey = factory(Key::class)->create();
        $thirdKey = factory(Key::class)->create();
        $fourthKey = factory(Key::class)->create();
        $company = factory(Company::class)->create();
        $thirdCompany = factory(Company::class)->create();
        $company->keys()->sync([$firstKey->{Key::ID}, $secondKey->{Key::ID}]);
        $secondCompany = factory(Company::class)->create();
        $secondCompany->keys()->sync([$firstKey->{Key::ID}, $thirdKey->{Key::ID}]);
        $this->assertTrue(
            Company::whereHasKeys([$firstKey->{Key::ID}, $secondKey->{Key::ID}])->first()->is($company)
        );
        $this->assertEquals(Company::whereHasKeys([$firstKey->{Key::ID}])->get()->count(), 2);
        $this->assertEquals(Company::whereHasKeys([])->get()->count(), 3);
        $this->assertNull(Company::whereHasKeys([$firstKey->{Key::ID}, $fourthKey->{Key::ID}])->first());
        $this->assertTrue(
            Company::whereKeysJustIn([$firstKey->{Key::ID}, $secondKey->{Key::ID}, $thirdKey->{Key::ID}])
                ->get()
                ->first()
                ->is($company)
        );
        $this->assertTrue(
            Company::whereKeysJustIn([$thirdKey->{Key::ID}, $secondKey->{Key::ID}])->first()->is($thirdCompany)
        );
        $this->assertTrue(
            Company::whereKeysJustIn([$fourthKey->{Key::ID}, $thirdKey->{Key::ID}])->first()->is($thirdCompany)
        );
        $this->assertEquals(Company::whereKeysJustIn([$firstKey->{Key::ID}])->count(), 1);
        $this->assertTrue(
            Company::whereKeysJustIn([$firstKey->id, $thirdKey->id,$secondKey->{Key::ID}])
                ->get()
                ->first()
                ->is($company)
        );
    }
}
