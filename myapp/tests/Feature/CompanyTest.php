<?php

namespace Tests\Feature;

use App\Company;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CompanyTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    /** @test */
    public function itShouldCreateNewPerson()
    {
        $title = $this->faker->word;
        $this->post(route('companies.store'), [Company::TITLE => $title])
            ->assertRedirect(route('companies.index'));
        $this->assertEquals(1, Company::all()->count());
    }
}
