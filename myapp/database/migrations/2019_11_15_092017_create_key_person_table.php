<?php

use App\Key;
use App\Person;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKeyPersonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('key_person', function (Blueprint $table) {
            $table->unsignedBigInteger('key_id');
            $table->unsignedBigInteger('person_id');

            $table->unique(['key_id', 'person_id']);

            $table->foreign('key_id')->references(Key::ID)->on(Key::TABLE);
            $table->foreign('person_id')->references(Person::ID)->on(Person::TABLE);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('key_person');
    }
}
