<?php

use App\Company;
use App\Key;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyKeyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_key', function (Blueprint $table) {
            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('key_id');

            $table->unique(['company_id', 'key_id']);

            $table->foreign('key_id')->references(Key::ID)->on(Key::TABLE);
            $table->foreign('company_id')->references(Company::ID)->on(Company::TABLE);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_key');
    }
}
