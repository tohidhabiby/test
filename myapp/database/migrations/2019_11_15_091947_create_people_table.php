<?php

use App\Person;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Person::TABLE, function (Blueprint $table) {
            $table->bigIncrements(Person::ID);
            $table->string(Person::FIRST_NAME);
            $table->string(Person::LAST_NAME);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Person::TABLE);
    }
}
