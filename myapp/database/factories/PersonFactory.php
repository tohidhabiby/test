<?php

/** @var Factory $factory */

use App\Person;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Person::class, function (Faker $faker) {
    return [
        Person::FIRST_NAME => $faker->firstName,
        Person::LAST_NAME => $faker->lastName
    ];
});
