<?php

/** @var Factory $factory */

use App\Key;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Key::class, function (Faker $faker) {
    return [
        Key::TITLE => $faker->word
    ];
});
