<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Route::get('/keys/create');
Route::resource('keys', 'KeyController')->except('show');
Route::resource('people', 'PersonController')->except('show');
Route::resource('companies', 'CompanyController')->except('show');
